import "./rx-extensions";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";

import {
    CreateEventComponent,
    CreateSessionComponent,
    DurationPipe,
    EventDetailsComponent,
    EventListResolver,
    EventResolver,
    EventService,
    EventsListComponent,
    EventThumbnailComponent,
    LocationValidator,
    SessionListComponent,
    UpvoteComponent,
    VoterService } from "./events/index";

import { CollapsableWellComponent,
    JQ_TOKEN,
    ModalTriggerDirective,
    SimpleModalComponent,
    Toastr,
    TOASTR_TOKEN} from "./common/index";
import { Error404Component } from "./errors/404.component";
import { EventsAppComponent } from "./events-app.component";
import { NavComponent } from "./nav/nav.component";
import { appRoutes } from "./routes";
import { AuthService } from "./user/auth.service";

declare let toastr: Toastr;
declare let jQuery: Object;

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,

        RouterModule.forRoot(appRoutes)
    ],
    declarations: [
        NavComponent,
        EventsAppComponent,
        EventsListComponent,
        EventThumbnailComponent,
        EventDetailsComponent,
        CreateEventComponent,
        CreateSessionComponent,
        SessionListComponent,
        CollapsableWellComponent,
        SimpleModalComponent,
        Error404Component,
        UpvoteComponent,
        ModalTriggerDirective,
        LocationValidator,
        DurationPipe],
    providers: [
        EventService,
        {
            provide: TOASTR_TOKEN,
            useValue: toastr,
        },
        {
            provide: JQ_TOKEN,
            useValue: jQuery,
        },
        AuthService,
        VoterService,
        EventResolver,
        EventListResolver,
        {
            provide: "canDeactivateCreateEvent",
            useValue: checkDirtyState,
        }
    ],
    bootstrap: [EventsAppComponent]
})
export class AppModule {}

function checkDirtyState(component: CreateEventComponent){
        // tslint:disable-next-line:max-line-length
        return component.isDirty ? window.confirm("You have not saved this event, do you really want to cancel?") : true;
}
