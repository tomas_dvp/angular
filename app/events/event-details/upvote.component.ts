import { Component, Input, EventEmitter, Output } from '@angular/core'
import { ISession } from '../index';

@Component({
    selector: "upvote",
    styleUrls: ['/app/events/event-details/upvote.component.css'],
    template:`
        <div class="votingWidgetContainer pointable" (click)="onClick()">
            <div class="well votingWidget">
                <div class="votingButton">
                    <i class="glyphicon glyphicon-heart"
                    [style.color]="iconColor"></i>
                </div>
            </div>
            <div class="badge badge-inverse votingCount">
                <div>{{count}}</div>
            </div>
        </div>
    `
})

export class UpvoteComponent{
    @Input() count: number;
    @Input() set voted(val){
        this.iconColor = val ? 'red' : 'white';
    }

    iconColor: string;
    @Output() vote = new EventEmitter();    

    onClick(){
        this.vote.emit({});
    }
}