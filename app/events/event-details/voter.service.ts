import { Injectable } from "@angular/core";
import { ISession } from "../index";
import { Observable } from 'rxjs/Observable';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class VoterService{

    constructor(private http:Http){}

    deleteVoter(eventId: number, session: ISession, userName: string){
        session.voters = session.voters.filter( x => x !== userName);

        this.http.delete(`/api/events/${eventId}/sessions/${session.id}/voters/${userName}`)
        .catch(this.handleError).subscribe();
    }

    addVoter(eventId: number, session: ISession, userName: string){
        session.voters.push(userName);

        let headers = new Headers({ 'Content-Type':'application/json'});
        let options = new RequestOptions({headers: headers});
        this.http.post(`/api/events/${eventId}/sessions/${session.id}/voters/${userName}`, null, options)
        .map((res: Response) =>{
            return res.json();
        }).catch(this.handleError).subscribe();
    }

    userHasVoted(session: ISession, userName: string){
        return session.voters.some( x => x === userName);
    }

    private handleError(error:Response){
        return Observable.throw(error.statusText);
      }
}