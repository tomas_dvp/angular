import { Component, OnInit } from '@angular/core'
import { EventService } from "./shared/event.service";
import { ActivatedRoute } from '@angular/router'

@Component({
    template: `
    <div> 
    <h1> Upcoming chess events </h1>
    <hr/>
    <div class="row">
        <div *ngFor="let event of events" class="col-md-5">
            <event-thumbnail [event]="event"></event-thumbnail>
        </div>
    </div>
    </div>
    `
})
export class EventsListComponent implements OnInit{
    events:any;
    
    constructor(private eventService:EventService, private router:ActivatedRoute)
    {}

    ngOnInit(){
        //this.events = this.router.snapshot.data['events'];
        this.eventService.getEvents().subscribe(events => { this.events = events });
    }
}