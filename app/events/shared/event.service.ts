import { Injectable, EventEmitter} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
//import "rxjs/add/operator/map";
// import "rxjs/add/operator/catch";

import { IEvent, ISession } from './event.model';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class EventService{

  //constructor(private http:Http){}

    getEvents():Subject<IEvent[]>{
      // return this.http.get("/api/events")
      //   .map((response: Response) => {
      //   return <IEvent[]>response.json();
      // }).catch(this.handleError);
      // locally
      let subject = new Subject<IEvent[]>();
      setTimeout(() => {
        subject.next(EVENTS);
        subject.complete();
      }, 100);
      return subject;      
    }

    getEvent(id:number):IEvent{//Observable<IEvent>{
      // return this.http.get("api/events/"+id)
      //   .map((response: Response) => {
      //     return <IEvent>response.json();
      //   }).catch(this.handleError);

      //locally
      return  EVENTS.find( x => x.id == id );
    }

    saveEvent(event){ // : Observable<IEvent>{
      // let headers = new Headers({'Content-Type':'application/json'});
      // let options : RequestOptions = new RequestOptions({headers: headers});

      // return this.http.post('/api/events', event, options)
      //   .map((response: Response)=>{
      //     return response.json();
      //   }).catch(this.handleError);
      event.id = 999;
      event.sessions = [];
      EVENTS.push(event);
    }

    updateEvent(event){
      let index = EVENTS.findIndex( x => x.id = event.id)
      EVENTS[index] = event
    }
    searchSessions(searchTerm:string){
      // return this.http.get("/api/sessions/search?search=" + searchTerm)
      // .map((response: Response) => {
      //   return response.json();
      // }).catch(this.handleError);
      var term = searchTerm.toLocaleLowerCase();
      var results: ISession[] = [];
  
      EVENTS.forEach(event => {
        var matchingSessions = event.sessions.filter(session => session.name.toLocaleLowerCase().indexOf(term) > -1);
        matchingSessions = matchingSessions.map((session:any) => {
          session.eventId = event.id;
          return session;
        })
        results = results.concat(matchingSessions);
      })
  
      var emitter = new EventEmitter(true);
      setTimeout(() => {
        emitter.emit(results);
      }, 100);
      return emitter;
    }

    private handleError(error:Response){
      return Observable.throw(error.statusText);
    }
}

const EVENTS: IEvent[] = [
    {
      id: 1,
      name: 'Flank openings',
      date: '9/26/2036',
      time: '10:00 am',
      price: 599.99,
      imageUrl: '/app/assets/images/ChessPics/3.jpg',
      location: {
        address: '1057 DT',
        city: 'London',
        country: 'England'
      },
      sessions: [
        {
          id: 1,
          name: "English Opening",
          presenter: "Magnus Carlsen",
          duration: 1,
          level: "Intermediate",
          abstract: `The English Opening is a chess opening that begins with the move:
          1. c4      
          A flank opening, it is the fourth most popular and, according to various databases, anywhere from one of the two most successful[1] to the fourth most successful of White's twenty possible first moves. White begins the fight for the centre by staking a claim to the d5-square from the wing, in hypermodern style. Although many lines of the English have a distinct character, the opening is often used as a transpositional device in much the same way as 1.Nf3 – to avoid such highly regarded responses to 1.d4 as the Nimzo-Indian and Grünfeld Defences, and is considered reliable and flexible.`,
          voters: ['voter1', 'voter2', 'voter3'],
          imageUrl: '/app/assets/images/ChessOpenings/FlankOpenings/EnglishOpening.png',
        },
        {
          id: 2,
          name: "Benoni Defense",
          presenter: "Garry Kasparov",
          duration: 1,
          level: "Intermediate",
          abstract: `The Benoni Defense is a chess opening characterized by the moves:
          1. d4 Nf6
          2. c4 c5
          3. d5      
          Black can then sacrifice a pawn by 3...b5 (the Benko Gambit), but if Black does not elect this line then 3...e6 is the most common move (though 3...d6 or 3...g6 are also seen, typically leading to main lines).`,
          voters: ['voter1', 'voter2', 'voter3', 'voter4'],
          imageUrl: '/app/assets/images/ChessOpenings/FlankOpenings/BenoniDefense.png',
        },
        {
          id: 3,
          name: "Dutch Defence",
          presenter: "Fabiano Caruana",
          duration: 2,
          level: "Advanced",
          abstract: `The Dutch Defence is a chess opening characterised by the moves:
          1. d4 f5      
          Black's 1...f5 stakes a serious claim to the e4-square and envisions an attack in the middlegame on White's kingside; however, it also weakens Black's kingside some (especially the e8–h5 diagonal)[1] and contributes nothing to Black's development. Like its 1.e4 counterpart, the Sicilian Defence, the Dutch is an aggressive and unbalancing opening, resulting in the lowest percentage of draws among the most common replies to 1.d4.[2] Through the ages White has tried all sorts of methods to exploit the kingside weaknesses, such as the Staunton Gambit (2.e4) and Korchnoi Attack (2.h3 and 3.g4), but Black's resources seem just about adequate.
          The Dutch has never been a main line against 1.d4 and is rarely seen at high level, although a number of top players, including Alexander Alekhine, Bent Larsen, Paul Morphy and Miguel Najdorf, have used it with success. Perhaps its high-water mark occurred in 1951 when both World Champion Mikhail Botvinnik and his challenger, David Bronstein, played it in their 1951 World Championship match. Among the world's top 10 players today, only Hikaru Nakamura is a consistent practitioner.`,
          voters: [],
          imageUrl: '/app/assets/images/ChessOpenings/FlankOpenings/DutchDefence.png',
        },
        {
          id: 4,
          name: "Réti Opening",
          presenter: "Levon Aronian",
          duration: 2,
          level: "Advanced",
          abstract: `The Réti Opening is a hypermodern chess opening whose traditional or classic method begins with the moves:
          1. Nf3 d5
          2. c4
          White plans to bring the d5-pawn under attack from the flank, or entice it to advance to d4 and undermine it later. White will couple this plan with a kingside fianchetto (g3 and Bg2) to create pressure on the light squares in the center.
          The opening is named after Richard Réti (1889–1929), an untitled Grandmaster from Czechoslovakia. The opening is in the spirit of the hypermodernism movement that Réti championed, with the center being dominated from the wings rather than by direct occupation.
          1.Nf3 develops the knight to a good square, prepares for quick castling, and prevents Black from occupying the center by 1...e5. White maintains flexibility by not committing to a particular central pawn structure, while waiting to see what Black will do. But the Réti should not be thought of as a single opening sequence, and certainly not a single opening move, but rather as an opening complex with many variations sharing common themes.
          In the Encyclopaedia of Chess Openings (ECO), Réti Opening is classified as codes A04–A09.`,
          voters: [],
          imageUrl: '/app/assets/images/ChessOpenings/FlankOpenings/RétiOpening.png',
        },
        {
          id: 5,
          name: "Benko Gambit",
          presenter: "Garry Kasparov",
          duration: 2,
          level: "Beginner",
          abstract: `The Benko Gambit (or Volga Gambit) is a chess opening characterised by the move 3...b5 in the Benoni Defense arising after:
          1. d4 Nf6
          2. c4 c5
          3. d5 b5
          The Encyclopaedia of Chess Openings (ECO) has three codes for the Benko Gambit:[1]
          A57 3...b5
          A58 3...b5 4.cxb5 a6 5.bxa6
          A59 3...b5 4.cxb5 a6 5.bxa6 Bxa6 6.Nc3 d6 7.e4`,
          voters: ['voter1', 'voter2'],
          imageUrl: '/app/assets/images/ChessOpenings/FlankOpenings/BenkoGambit.png',
        },
        {
          id: 6,
          name: "Old Indian Defense",
          presenter: "Garry Kasparov",
          duration: 2,
          level: "Beginner",
          abstract: `The Old Indian Defense is a chess opening defined by the moves:
          1. d4 Nf6
          2. c4 d6
          This opening is distinguished from the King's Indian Defense in that Black develops his king's bishop on e7 rather than by fianchetto on g7. Mikhail Chigorin pioneered this defense late in his career.
          The Old Indian is considered sound, though developing the bishop at e7 is less active than the fianchetto, and it has never attained the popularity of the King's Indian. Some King's Indian players will use the Old Indian to avoid certain anti-King's Indian systems, such as the Sämisch and Averbakh Variations.
          The opening is classified in the Encyclopaedia of Chess Openings with the codes A53–A55.`,
          voters: ['voter1'],
          imageUrl: '/app/assets/images/ChessOpenings/FlankOpenings/OldIndianDefense.png',
        },
        {
          id: 7,
          name: "Bird's Opening",
          presenter: "Garry Kasparov",
          duration: 2,
          level: "Beginner",
          abstract: `Bird's Opening (or the Dutch Attack) is a chess opening characterised by the move:
          1. f4
          Bird's is a standard flank opening. White's strategic ideas involve control of the e5-square, offering good attacking potential at the expense of slightly weakening his kingside. Black may challenge White's plan to control e5 immediately by playing From's Gambit (1...e5); however, the From's Gambit is notoriously double-edged and should only be played after significant study.
          The Encyclopaedia of Chess Openings assigns two codes for Bird's Opening: A02 (1.f4) and A03 (1.f4 d5).`,
          voters: ['voter1'],
          imageUrl: "/app/assets/images/ChessOpenings/FlankOpenings/Bird's Opening.png",
        }
      ]
    },
    {
      id: 2,
      name: 'Semi-Open Games',
      date: '4/15/2037',
      time: '9:00 am',
      price: 950.00,
      imageUrl: '/app/assets/images/ChessPics/4.jpg',
      location: {
        address: 'The Chess Center',
        city: 'Amsterdam',
        country: 'Netherlands'
      },
      sessions: [
        {
          id: 1,
          name: "Sicilian Defence",
          presenter: "Maxime Vachier-Lagrave",
          duration: 4,
          level: "Beginner",
          abstract: `The Sicilian Defence is a chess opening that begins with the following moves:
          1. e4 c5
          The Sicilian is the most popular and best-scoring response to White's first move 1.e4. 1.d4 is a statistically more successful opening for White due to the high success rate of the Sicilian defence against 1.e4.[1] New In Chess stated in its 2000 Yearbook that of the games in its database, White scored 56.1% in 296,200 games beginning 1.d4, but 54.1% in 349,855 games beginning 1.e4, mainly due to the Sicilian, which held White to a 52.3% score in 145,996 games.[2]
          17% of all games between grandmasters, and 25% of the games in the Chess Informant database, begin with the Sicilian.[3] Almost one quarter of all games use the Sicilian Defence.[4]`,
          voters: ['voter1', 'voter2'],
          imageUrl: '/app/assets/images/ChessOpenings/Semi-Open Games other than the French Defense/Sicilian Defence.png',
        },
        {
          id: 2,
          name: "Caro–Kann Defence",
          presenter: "Viswanathan Anand",
          duration: 3,
          level: "Intermediate",
          abstract: `The Caro–Kann Defence is a chess opening characterised by the moves:
          1. e4 c6
          The Caro–Kann is a common defence against the King's Pawn Opening and is classified as a "Semi-Open Game" like the Sicilian Defence and French Defence, although it is thought to be more solid and less dynamic than either of those openings. It often leads to good endgames for Black, who has the better pawn structure.`,
          voters: ['voter1', 'voter2', 'voter3'],
          imageUrl: '/app/assets/images/ChessOpenings/Semi-Open Games other than the French Defense/Caro–Kann Defence.png',
        },
        {
          id: 3,
          name: "Pirc Defence",
          presenter: "Vladimir Kramnik",
          duration: 2,
          level: "Intermediate",
          abstract: `The Pirc Defence (correctly pronounced "peerts", but often mispronounced "perk"), sometimes known as the Ufimtsev Defence or Yugoslav Defence, is a chess opening characterised by Black responding to 1.e4 with 1...d6 and 2...Nf6, followed by ...g6 and ...Bg7, while allowing White to establish an impressive-looking centre with pawns on d4 and e4. It is named after the Slovenian Grandmaster Vasja Pirc.`,
          voters: ['voter1'],
          imageUrl: '/app/assets/images/ChessOpenings/Semi-Open Games other than the French Defense/Pirc Defence.png',
        },
        {
          id: 4,
          name: "Alekhine's Defence",
          presenter: "Garry Kasparov",
          duration: 1,
          level: "Beginner",
          abstract: `Alekhine's Defence is a chess opening which begins with the moves:
          1. e4 Nf6
          Black tempts White's pawns forward to form a broad pawn centre, with plans to undermine and attack the white structure later in the spirit of hypermodern defence. White's imposing mass of pawns in the centre often includes pawns on c4, d4, e5, and f4. Grandmaster Nick de Firmian observes of Alekhine's Defence in MCO-15 (2008), "The game immediately loses any sense of symmetry or balance, which makes the opening a good choice for aggressive fighting players."[1]`, 
          voters: ['voter1'],
          imageUrl: "/app/assets/images/ChessOpenings/Semi-Open Games other than the French Defense/Alekhine's Defence.png",
        },
        {
          id: 5,
          name: "Modern Defense",
          presenter: "Garry Kasparov",
          duration: 1,
          level: "Beginner",
          abstract: `The Modern Defense (also known as the Robatsch Defence after Karl Robatsch) is a hypermodern chess opening in which Black allows White to occupy the center with pawns on d4 and e4, then proceeds to attack and undermine this "ideal" center without attempting to occupy it himself. The opening has been most notably used by British grandmasters Nigel Davies and Colin McNab.
          The Modern Defense is closely related to the Pirc Defence, the primary difference being that in the Modern, Black delays developing his knight to f6. The delay of ...Nf6 attacking White's pawn on e4 gives White the option of blunting the g7-bishop with c2–c3. There are numerous transpositional possibilities between the two openings.`, 
          voters: ['voter1'],
          imageUrl: "/app/assets/images/ChessOpenings/Semi-Open Games other than the French Defense/Modern Defense.png",
        },
        {
          id: 6,
          name: "Scandinavian Defense",
          presenter: "Garry Kasparov",
          duration: 1,
          level: "Beginner",
          abstract: `The Scandinavian Defense (or Center Counter Defense, or Center Counter Game) is a chess opening characterized by the moves:
          1. e4 d5 `, 
          voters: ['voter1'],
          imageUrl: "/app/assets/images/ChessOpenings/Semi-Open Games other than the French Defense/Scandinavian Defense.png",
        },
      ]
    },
    {
      id: 3,
      name: 'Open Games',
      date: '5/4/2037',
      time: '9:00 am',
      price: 759.00,
      imageUrl: '/app/assets/images/ChessPics/a.jpg',
      location: {
        address: 'The Palatial America Hotel',
        city: 'Salt Lake City',
        country: 'USA'
      },
      sessions: [
        {
          id: 1,
          name: "Ruy Lopez",
          presenter: "Hikaru Nakamura",
          duration: 2,
          level: "Intermediate",
          abstract: `The Ruy Lopez (/rʊ.ɪ ˈloʊpɛz/; Spanish pronunciation: [ˈruj ˈlopeθ/ˈlopes]), also called the Spanish Opening or Spanish Game, is a chess opening characterised by the moves:
          1. e4 e5
          2. Nf3 Nc6
          3. Bb5
          The Ruy Lopez is named after 16th-century Spanish bishop Ruy López de Segura. It is one of the most popular openings, with such a vast number of variations that in the Encyclopaedia of Chess Openings (ECO) all codes from C60 to C99 are assigned to them.`,
          voters: ['voter1', 'voter2', 'voter3'],
          imageUrl: "/app/assets/images/ChessOpenings/Open Games and the French Defense/Ruy Lopez.png",          
        },
        {
          id: 2,
          name: "French Defence",
          presenter: "Alexander Grischuk",
          duration: 2,
          level: "Intermediate",
          abstract: `The French Defence is a chess opening characterised by the moves:
          1. e4 e6
          This is most commonly followed by 2.d4 d5, with Black intending ...c5 at a later stage, attacking White's centre and gaining space on the queenside. White has extra space in the centre and on the kingside and often plays for a breakthrough with f4–f5. The French has a reputation for solidity and resilience, although some lines such as the Winawer Variation can lead to sharp complications. Black's position is often somewhat cramped in the early game; in particular, the pawn on e6 can impede the development of the bishop on c8.`,
          voters: ['voter1', 'voter2'],
          imageUrl: "/app/assets/images/ChessOpenings/Open Games and the French Defense/French Defence.png",
        },
        {
          id: 3,
          name: "Petrov's Defence",
          presenter: "Shakhriyar Mamedyarov",
          duration: 1,
          level: "Intermediate",
          abstract: `Petrov's Defence or the Petrov Defence (also called Petroff's Defence, Russian Defence, and Russian Game) is a chess opening characterised by the following moves:
          1. e4 e5
          2. Nf3 Nf6
          Though this symmetrical response has a long history, it was first popularised by Alexander Petrov, a Russian chess player of the mid-19th century. In recognition of the early investigations by the Russian masters Petrov and Carl Jaenisch, this opening is called the Russian Game in some countries.`,
          voters: ['voter1', 'voter2', 'voter3'],
          imageUrl: "/app/assets/images/ChessOpenings/Open Games and the French Defense/Petrov's Defence.png",
        },
        {
          id: 4,
          name: "Vienna Game",
          presenter: "Anish Giri",
          duration: 1,
          level: "Beginner",
          abstract: `The Vienna Game is an opening in chess that begins with the moves:
          1. e4 e5
          2. Nc3
          White's second move is less common than 2.Nf3, and is also more recent. A book reviewer wrote in the New York Times in 1888 that "since Morphy only one new opening has been introduced, the 'Vienna'.`,
          voters: ['voter1', 'voter2', 'voter3', 'voters4'],
          imageUrl: "/app/assets/images/ChessOpenings/Open Games and the French Defense/Vienna Game.png",
        },
        {
          id: 5,
          name: "Center Game",
          presenter: "Teimour Radjabov",
          duration: 2,
          level: "Beginner",
          abstract: `The Center Game is a chess opening that begins with the moves:
          1. e4 e5
          2. d4 exd4
          The game usually continues 3.Qxd4 Nc6, developing with a gain of tempo for Black due to the attack on the white queen. (Note that 3.c3 is considered a separate opening: the Danish Gambit.)`,
          voters: ['voter1', 'voter2'],
          imageUrl: "/app/assets/images/ChessOpenings/Open Games and the French Defense/Center Game.png",
        },
        {
          id: 6,
          name: "King's Gambit",
          presenter: "Alexander Morozevich",
          duration: 2,
          level: "Intermediate",
          abstract: `The King's Gambit is a chess opening that begins with the moves:
          1. e4 e5
          2. f4
          White offers a pawn to divert the black e-pawn. If Black accepts the gambit, White has two main plans. The first is to play d4 and Bxf4, regaining the gambit pawn with central domination. The alternative plan is to play Nf3 and Bc4 followed by 0-0, when the semi-open f-file allows White to barrel down onto the weakest point in Black's position, the pawn on f7. Theory has shown that in order for Black to maintain the gambit pawn, he may well be forced to weaken his kingside, with moves such as ...g5 or odd piece placement (e.g. ...Nf6–h5). A downside to the King's Gambit is that White weakens his own king's position, exposing it to the latent threat of ...Qh4+ (or ...Be7–h4+). With a black pawn on f4, White cannot usually respond to the check with g3, but if the king is forced to move then it also loses the right to castle.`,
          voters: ['voter1', 'voter2'],
          imageUrl: "/app/assets/images/ChessOpenings/Open Games and the French Defense/King's Gambit.png",
        },
        {
          id: 7,
          name: "Philidor Defence",
          presenter: "Alexander Morozevich",
          duration: 2,
          level: "Intermediate",
          abstract: `The Philidor Defence is a chess opening characterised by the moves:
          1. e4 e5
          2. Nf3 d6
          The opening is named after the famous 18th-century player François-André Danican Philidor, who advocated it as an alternative to the common 2...Nc6. His original idea was to challenge White's centre by the pawn thrust ...f7–f5.`,
          voters: ['voter1', 'voter2'],
          imageUrl: "/app/assets/images/ChessOpenings/Open Games and the French Defense/Philidor Defence.png",
        },
      ]
    },
    {
      id: 4,
      name: 'Closed Games',
      date: '6/10/2037',
      time: '8:00 am',
      price: 800.00,
      imageUrl: '/app/assets/images/ChessPics/b.jpg',
      location: {
        address: 'The UN Angular Center',
        city: 'New York',
        country: 'USA'
      },
      sessions: [
        {
          id: 1,
          name: "Queen's Gambit",
          presenter: "Sergey Karjakin",
          duration: 2,
          level: "Beginner",
          abstract: `The Queen's Gambit is a chess opening that starts with the moves:
          1. d4 d5
          2. c4      
          The Queen's Gambit is one of the oldest known chess openings. It was mentioned in the Göttingen manuscript of 1490 and was later analysed by masters such as Gioachino Greco in the 17th century. In the 18th century, it was recommended by Phillip Stamma and is sometimes known as the Aleppo Gambit in his honour.[1] During the early period of modern chess, queen pawn openings were not in fashion and the Queen's Gambit did not become common until the 1873 tournament in Vienna.`,
          voters: ['voter1', 'voter2'],
          imageUrl: "/app/assets/images/ChessOpenings/Closed Games and Semi-Closed Games/Queen's Gambit.png",
        },
        {
          id: 2,
          name: "Grünfeld Defence",
          presenter: "US Secretary of State Zach Galifianakis",
          duration: 2,
          level: "Beginner",
          abstract: `The Grünfeld Defence (ECO codes D70–D99) is a chess opening characterised by the moves:
          1. d4 Nf6
          2. c4 g6
          3. Nc3 d5
          Black offers White the possibility of cxd5, when after Nxd5 White further gets the opportunity to kick the Black Knight around with e4, leading to an imposing central pawn duo for White. If White does not take the d5 pawn, Black may eventually play dxc4, when a White response of e4 again leads to the same pawn structure. In classical opening theory this imposing pawn centre was held to give White a large advantage, but the hypermodern school, which was coming to the fore in the 1920s, held that a large pawn centre could be a liability rather than an asset. The Grünfeld is therefore a key hypermodern opening, showing in stark terms how a large pawn centre can either be a powerful battering ram or a target for attack.`,
          voters: ['voter1', 'voter2', 'voter3'],
          imageUrl: "/app/assets/images/ChessOpenings/Closed Games and Semi-Closed Games/Grünfeld Defence.png",
        },
        {
          id: 3,
          name: "Blackmar–Diemer Gambit",
          presenter: "Vassily Ivanchuk",
          duration: 3,
          level: "Advanced",
          abstract: `The Blackmar–Diemer Gambit (or BDG) is a chess opening characterized by the moves:
          1. d4 d5
          2. e4 dxe4
          3. Nc3
          where White intends to follow up with f2–f3, usually on the fourth move. White obtains a move and a half-open f-file in return for a pawn, and as with most gambits, White aims to achieve rapid development and active posting of his pieces in order to rapidly build up an attack at the cost of the gambit pawn. It is one of the very few gambits available to White after 1.d4.[1]`,
          voters: ['voter1', 'voter2'],
          imageUrl: "/app/assets/images/ChessOpenings/Closed Games and Semi-Closed Games/Blackmar–Diemer Gambit.png",
        },
      ]
    },
    {
      id: 5,
      name: 'Indian Defenses',
      date: '2/10/2037',
      time: '9:00 am',
      price: 400.00,
      imageUrl: '/app/assets/images/ChessPics/c.jpg',
      location: {
        address: 'The Excalibur',
        city: 'Las Vegas',
        country: 'USA'
      },
      sessions: [
        {
          id: 1,
          name: "Nimzo-Indian Defence",
          presenter: "Bobby Fischer",
          duration: 1,
          level: "Intermediate",
          abstract: `The Nimzo-Indian Defence is a chess opening characterised by the moves:
          1. d4 Nf6
          2. c4 e6
          3. Nc3 Bb4
          Other move orders, such as 1.c4 e6 2.Nc3 Nf6 3.d4 Bb4, are also feasible. In the Encyclopaedia of Chess Openings, the Nimzo-Indian is classified as E20–E59.`,
          voters: ['voter1', 'voter2'],
          imageUrl: "/app/assets/images/ChessOpenings/Indian Defenses/Nimzo-Indian Defence.png",
        },
        {
          id: 2,
          name: "Queen's Indian Defense",
          presenter: "Anatoly Karpov",
          duration: 2,
          level: "Beginner",
          abstract: `The Queen's Indian Defense[1] (QID) is a chess opening defined by the moves:
          1. d4 Nf6
          2. c4 e6
          3. Nf3 b6
          The opening is a solid defense to the Queen's Pawn Game.[2] 3...b6 increases Black's control over the central light squares e4 and d5 by preparing to fianchetto the queen's bishop, with the opening deriving its name from this maneuver. As in the other Indian defenses, Black attempts to control the center with pieces, instead of occupying it with pawns in classical style.`,
          voters: ['voter1', 'voter2', 'voters3'],
          imageUrl: "/app/assets/images/ChessOpenings/Indian Defenses/Queen's Indian Defense.png",
        },
        {
          id: 3,
          name: "King's Indian Defence",
          presenter: "Anatoly Karpov",
          duration: 2,
          level: "Beginner",
          abstract: `The King's Indian Defence is a common chess opening. It arises after the moves:
          1. d4 Nf6
          2. c4 g6
          Black intends to follow up with 3...Bg7 and 4...d6 (the Grünfeld Defence arises when Black plays 3...d5 instead, and is considered a separate opening). White's major third move options are 3.Nc3, 3.Nf3 or 3.g3, with both the King's Indian and Grünfeld playable against these moves. The Encyclopaedia of Chess Openings classifies the King's Indian Defence under the codes E60 through E99.`,
          voters: ['voter1', 'voter2', 'voters3'],
          imageUrl: "/app/assets/images/ChessOpenings/Indian Defenses/King's Indian Defence.png",
        },
        {
          id: 4,
          name: "Catalan Opening",
          presenter: "Anatoly Karpov",
          duration: 2,
          level: "Beginner",
          abstract: `The Catalan is a chess opening where White adopts a combination of the Queen's Gambit and Réti Opening: White plays d4 and c4 and fianchettoes the white bishop on g2. A common opening sequence is 1.d4 Nf6 2.c4 e6 3.g3, although the opening can arise from a large number of move orders (see transposition). ECO codes E01–E09 are for lines with 1.d4 Nf6 2.c4 e6 3.g3 d5 4.Bg2, and others are part of E00.`,
          voters: ['voter1', 'voter2', 'voters3'],
          imageUrl: "/app/assets/images/ChessOpenings/Indian Defenses/Catalan Opening.png",
        }
      ]
    }
]

